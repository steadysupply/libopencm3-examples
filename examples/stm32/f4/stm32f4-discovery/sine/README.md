# README

This example demonstrates using the oboard audio DAC on
STM32F4-Discovery board.
It plays a continuous 1 kHz sine wave.

## Board connections

Sepeakers attached to the 3.5mm audio jack.
Note: volume is turned low, but beware of loud sounds
in case of bugs (i.e. plug the headphones to your ear
only after playback starts!)
